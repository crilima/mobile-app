import { Component, Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable, from} from 'rxjs';
import {map} from 'rxjs/operators';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
    providedIn: 'root'
})

export class FollowingService{

url = "http://api.thegame.fail";

constructor(public http: HttpClient) {}  

getFollowing(userid): Observable<any>{
  console.log("service user id following",userid);
  return this.http.get(`http://api.thegame.fail/users/followings/${userid}`, {})
 
}
  
}

