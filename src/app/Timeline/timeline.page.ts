import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';
import { TimelineService } from '../Services/timeline.service';
import { PostModalService } from '../Services/postmodal.service';
import { ModalController } from '@ionic/angular';
import { PostModalPage } from '../post/postmodal.page';
import {ComponentProps, ModalOptions} from '@ionic/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-timeline',
  templateUrl: 'timeline.page.html',
  styleUrls: ['timeline.page.scss']
})
export class TimelinePage implements OnInit{

  results
  constructor(private TimelineService: TimelineService,private router: Router, private PostModalService: PostModalService, public ModalController: ModalController) {}
  ngOnInit(){
    console.log('initiated');
    this.getTimeline();
  }
  async getTimeline(){
    this.TimelineService.getTimeline()
    .subscribe(
      (data : Response) => {
      this.results = data;        
        console.log('get timeline',this.results);
      }
     )
  };
  
  getPost(item:any){
    this.router.navigate([`tabs/timeline/post/${item.id}`]);
    
  }

}
