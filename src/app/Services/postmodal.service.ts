import { Component, Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable, from} from 'rxjs';
import {map} from 'rxjs/operators';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
    providedIn: 'root'
})

export class PostModalService{

url = "http://api.thegame.fail";
constructor(public http: HttpClient) {}  

getPost(PostId): Observable<any>{
  console.log("service post id", PostId);
  return this.http.get(`http://api.thegame.fail/posts/${PostId}`, {})
 
}
  
}

