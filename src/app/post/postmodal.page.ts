import { Component, OnInit, Input } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';
import { PostModalService } from '../Services/postmodal.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-postmodal',
  templateUrl: 'postmodal.page.html',
  styleUrls: ['postmodal.page.scss']
})
export class PostModalPage  implements OnInit{

  public PostId: string;
  results 
  constructor(private PostModalService: PostModalService,public route: ActivatedRoute) {
  this.PostId = this.route.snapshot.params.id;

  }
  ngOnInit(){
    console.log('initiated');
    this.getPost();
  }
  async getPost(){         
     this.PostModalService.getPost(this.PostId).subscribe(
      (data : Response) => {
        this.results = data;        
        console.log('get post',this.results);
      }
     )
    
  };
}
