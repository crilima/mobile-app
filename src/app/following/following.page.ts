import { Component, OnInit, Input } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';
import { FollowingService } from '../Services/following.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-following',
  templateUrl: 'following.page.html',
  styleUrls: ['following.page.scss']
})
export class followingPage  implements OnInit{

  public userId: string;
  users 
  constructor(private FollowingService: FollowingService,public route: ActivatedRoute) {
  this.userId = this.route.snapshot.params.id;

  }
  ngOnInit(){
    console.log('initiated');
    this.getFollowing();
  }
  async getFollowing(){         
     this.FollowingService.getFollowing(this.userId).subscribe(
      (data : Response) => {
        this.users = data;        
        console.log('get following',this.users);
      }
     )
    
  };
}
