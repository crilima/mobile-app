import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';
import { AuthService } from '../Services/auth.service';
import {ComponentProps, ModalOptions} from '@ionic/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: 'auth.page.html',
  styleUrls: ['auth.page.scss']
})
export class AuthPage implements OnInit{

user = {id : ""}
results
  constructor(private AuthService: AuthService,private router: Router) {}
  ngOnInit(){
    console.log('initiated');
  }

 getUser(){
   console.log('userid form',this.user.id);
   
   this.AuthService.getUser(this.user.id)
   .subscribe(
     (data : Response) =>{
       this.results = data;
       console.log('user ', this.results);
       
     }
   )
 }

}
