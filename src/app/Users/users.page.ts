
import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';
import { UsersService } from '../Services/users.service';
import {ComponentProps, ModalOptions} from '@ionic/core';
import { Router } from "@angular/router";
@Component({
  selector: 'app-users',
  templateUrl: 'users.page.html',
  styleUrls: ['users.page.scss']
})
export class UsersPage implements OnInit{

  userid = "dbcc7b8e-4132-4f7b-8537-2e8ffe44ae9c";

  users
  constructor(private UsersService: UsersService,private router: Router) {}


  ngOnInit(){
    console.log('initiated');
    this.getUsers();
  }
  async getUsers(){

    this.UsersService.getUsers()
    .subscribe(
      (data : Response) => {
      this.users = data;        
        console.log('get users',this.users);
      }
     )
  };
  follow(item:any){
    //follow call
  }
  getFollowing(){
    this.router.navigate([`tabs/users/following/${this.userid}`]);
    
  }
}
